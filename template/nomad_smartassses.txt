job "{{ smartasses_job_name }}" {
  datacenters = ["{{ dcname }}"]

  type = "batch"

  periodic {
    cron             = "0 0 31 2 *"
    prohibit_overlap = true
  }
  
  constraint {
    attribute = "${attr.kernel.name}"
    value     = "{{ os }}"
  }
  
  group "{{ smartasses_job_name }}" {
    count = 1
    reschedule {
      attempts = 0
    }
    restart {
      attempts = 0
      mode     = "fail"
    }
    task "smartasses" {
      driver = "raw_exec"
	  resources {
	    cpu    = {{ cpu }}
	    memory = {{ memory }}
	  }
      logs {
        max_files     = 10
        max_file_size = 10
      }	  
      config {
        command = "{{ xcppath }}"
        args    = ["{{ args }}"]
      }
    }
  }
}
